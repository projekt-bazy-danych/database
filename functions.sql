--###FUNCTIONS###
-- Funkcja pobierajaca aktualna date z systemu i zwracajaca ja na wyjsciu
CREATE OR REPLACE FUNCTION Current_DateTime RETURN VARCHAR2
AS
    BEGIN
     RETURN TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS');
    END;
/

-- Funkcja podobna do powyzszej, lecz dodaje 1 tydzien (okres trwania Premium)
CREATE OR REPLACE FUNCTION Premium_End_Date RETURN VARCHAR2
AS
    BEGIN
     RETURN TO_CHAR(SYSDATE+7, 'YYYY-MM-DD HH24:MI:SS');
    END;
/

-- Procedura losujaca przedmioty o okreslonych prawdopowobienstwach
-- common -> 75% [0]
-- rare -> 19% [1]
-- epic -> 5% [2]
-- legendary -> 1% [3]
CREATE OR REPLACE FUNCTION Spin RETURN NUMBER
AS
    rarity NUMBER;
    i NUMBER;
BEGIN
	
        SELECT round(dbms_random.value(1,100)) INTO i FROM dual;
       
            IF i <= 75 THEN
                rarity := 0; --'Common'
            ELSIF i > 75 and i <= 94 THEN
                rarity := 1; --'RARE'
            ELSIF i > 94 and i <= 99 THEN
                rarity := 2; --'EPIC'
            ELSIF i = 100 THEN
                rarity := 3;--'LEGENDARY'
            END IF;
            
        RETURN rarity;    
    END;
/

--Funkcja do tworzenia skrytki nowego uzytkownika i zwracajaca ID tej skrytki
CREATE OR REPLACE FUNCTION Add_Deposit_Box RETURN NUMBER
AS
    deposit_box_id NUMBER;
BEGIN
    INSERT INTO depositboxes(nofreeslots) VALUES (default);
    SELECT depositboxid INTO deposit_box_id FROM depositboxes WHERE rownum = 1
        ORDER BY depositboxid DESC;
    
    RETURN deposit_box_id;
END;
/

--Funkcja do tworzenia nowego uzytkownika z podanym loginem i haslem
CREATE OR REPLACE FUNCTION Add_Pyrite_User(
    p_login VARCHAR,
    p_passwd VARCHAR
    ) RETURN NUMBER
AS
    user_id NUMBER;
    deposit_box_id NUMBER;
BEGIN
    deposit_box_id := add_deposit_box;
    INSERT INTO pyriteusers(login, userpassword, depositboxid, signupdate)
        values (p_login, p_passwd, deposit_box_id, sysdate);
    SELECT userid INTO user_id FROM pyriteusers WHERE rownum = 1
        ORDER BY userid DESC;

    RETURN user_id;
END;
/

CREATE OR REPLACE FUNCTION Add_Item(
    p_name VARCHAR,
    p_rarity NUMBER,
    p_value NUMBER
    ) RETURN NUMBER
AS
    item_id NUMBER;
BEGIN
    INSERT INTO items(itemname, rarity, itemvalue)
        values (p_name, p_rarity, p_value);
    SELECT itemid INTO item_id FROM items WHERE rownum = 1
        ORDER BY itemid DESC;

    RETURN item_id;
END;
/