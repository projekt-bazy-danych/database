drop table "JAKUB"."DEPOSITBOXES" cascade constraints;
drop table "JAKUB"."ITEMS" cascade constraints;
drop table "JAKUB"."OWNED" cascade constraints;
drop table "JAKUB"."PREMIUMS" cascade constraints;
drop table "JAKUB"."PYRITEUSERS" cascade constraints;
drop table "JAKUB"."SHOPTRANSACTIONS" cascade constraints;
drop table "JAKUB"."SPINS" cascade constraints;

purge recyclebin;