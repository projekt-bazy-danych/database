--###CREATING TABLES###
-- stworzenie podstawowych tabel
create table PyriteUsers (
UserID number generated always as identity,
Login varchar(32) not null,
UserPassword varchar(32) not null,
Credits number default 100,
SpinTotal number default 0,
DepositBoxID number not null,
SignUpDate date not null,
IsPremium number(1) default 0 not null);

create table Items (
ItemID number generated always as identity,
ItemName varchar(64) not null,
Rarity number not null,
ItemValue number not null);

create table Owned (
DepositBoxID number not null,
Amount number not null,
ItemID number not null);

create table DepositBoxes (
DepositBoxID number generated always as identity,
NOFreeSlots number default 10,
NOItems number default 0);

create table Spins (
SpinID number generated always as identity,
UserID number not null,
ItemID number not null);

create table Premiums (
UserID number not null,
PurchaseDate date not null);

create table ShopTransactions (
TransactionID number generated always as identity,
UserID number not null,
ItemID number not null,
Credits number not null);


--###RELATIONS###
    --###PRIMARY KEYS###
alter table pyriteusers add constraint pk_pyriteusers
primary key (userid);
alter table items add constraint pk_items
primary key (itemid);
alter table depositboxes add constraint pk_depositboxes
primary key (depositboxid);
alter table spins add constraint pk_spins
primary key (spinid);
alter table shoptransactions add constraint pk_shoptransactions
primary key (transactionid);

    --###FOREIGN KEYS###
      --PYRITEUSERS
alter table pyriteusers add constraint fk_user_depositbox
foreign key (depositboxid) references depositboxes(depositboxid);
      --OWNED
alter table owned add constraint fk_owned_depositbox
foreign key (depositboxid) references depositboxes(depositboxid);
alter table owned add constraint fk_owned_item
foreign key (itemid) references items(itemid);
      --SPINS
alter table spins add constraint fk_spin_user
foreign key (userid) references pyriteusers(userid);
alter table spins add constraint fk_spin_item
foreign key (itemid) references items(itemid);
      --PREMIUMS
alter table premiums add constraint fk_premium_user
foreign key (userid) references pyriteusers(userid);
      --SHOPTRANSACTIONS
alter table shoptransactions add constraint fk_transaction_user
foreign key (userid) references pyriteusers(userid);
alter table shoptransactions add constraint fk_transaction_item
foreign key (itemid) references items(itemid);